﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharClassLib
{
    public class Assasin : Thief
    {

        private String ultimateAbility;

        public Assasin(String name, int hp, int mp, String armorRating) : base(name, hp, mp, armorRating)
        {
            UltimateAbility = "Power of edge";
        }

        public string UltimateAbility { get => ultimateAbility; set => ultimateAbility = value; }

        public void useUltimate()
        {
            Console.WriteLine($"You used {UltimateAbility}");
            Console.WriteLine("~~~~~~~~~~~~~~~~~~~~~~~~~~~");
            Console.WriteLine($"CRAAAWLIING IN MY SKIIIN");
            Console.WriteLine("~~~~~~~~~~~~~~~~~~~~~~~~~~~");

        }
    }

}
