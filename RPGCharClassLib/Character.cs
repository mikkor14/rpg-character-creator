﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharClassLib
{
    public class Character
    {
        private String name;
        private int hp;
        private int mp;
        private String armorRating;

        public Character(String name, int hp, int mp, String armorRating)
        {
            this.Name = name;
            this.Hp = hp;
            this.Mp = mp;
            this.ArmorRating = armorRating;

        }

        public string Name { get => name; set => name = value; }
        public int Hp { get => hp; set => hp = value; }
        public int Mp { get => mp; set => mp = value; }
        public string ArmorRating { get => armorRating; set => armorRating = value; }


        public String printSummary()
        {
            return $"Character name: {Name} \n " +
                $"HP & MP: {Hp},{Mp} " +
                $"Armor rating: {ArmorRating} ";
        }

        public void attack(String target)
        {
            Console.WriteLine($"You attacked {target}");
        }

        public void move(String direction)
        {
            Console.WriteLine($"You moved {direction}");

        }

    }
}
