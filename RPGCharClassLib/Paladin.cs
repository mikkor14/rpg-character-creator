﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharClassLib
{
    public class Paladin : Warrior
    {
        private String ultimateAbility;

        public Paladin(String name, int hp, int mp, String armorRating) : base(name, hp, mp, armorRating)
        {
            UltimateAbility = "Holy purge";
        }

        public string UltimateAbility { get => ultimateAbility; set => ultimateAbility = value; }

        public void useUltimate()
        {
            Console.WriteLine($"You used {UltimateAbility}");
            Console.WriteLine("~~~~~~~~~~~~~~~~~~~~~~~~~~~");
            Console.WriteLine($"No swearing on my Christian server!");
            Console.WriteLine("~~~~~~~~~~~~~~~~~~~~~~~~~~~");

        }


    }

}
