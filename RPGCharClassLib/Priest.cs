﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharClassLib
{
    public class Priest : Wizzard
    {

        private String ultimateAbility;
        public Priest(String name, int hp, int mp, String armorRating) : base(name, hp, mp, armorRating)
        {
            UltimateAbility = "Apply bandaid";
        }

        public string UltimateAbility { get => ultimateAbility; set => ultimateAbility = value; }

        public void useUltimate()
        {
            Console.WriteLine($"You used {UltimateAbility}");
            Console.WriteLine("~~~~~~~~~~~~~~~~~~~~~~~~~~~");
            Console.WriteLine($"That will be 999$ plus insurance");
            Console.WriteLine("~~~~~~~~~~~~~~~~~~~~~~~~~~~");

        }
    }

}
