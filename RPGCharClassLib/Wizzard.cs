﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharClassLib
{
    public class Wizzard : Character
    {

        private List<String> skills;

        public List<string> Skills { get => skills; set => skills = value; }

        public Wizzard(String name, int hp, int mp, String armorRating) : base(name, hp, mp, armorRating)
        {
            Skills = new List<String> { "Magic missile", "288 Hellfire missile", "Conjure Corgi" };
        }


        public void useSkill(String skillname)
        {
            if (Skills.Contains(skillname))
            {
                Console.WriteLine($"You used {skillname}");
            }
            else
            {
                Console.WriteLine("You do not have any skills like that");
            }

        }

        new public String printSummary()
        {
            return $"Character name: {Name} \n " +
         $"HP & MP: {Hp},{Mp} \n" +
         $" Armor rating: {ArmorRating} \n " +
         $"Subclass: Warrior \n";

        }

        public String printSkills()
        {
            String skillsString = "List of skills \n" + "--------------------- \n";

            foreach (String skill in Skills)
            {
                skillsString += skill + "\n";
            }
            skillsString += "---------------------";

            return skillsString;
        }

    }

}
